package me.seroperson.gate.forhonor.global

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_global.*
import me.seroperson.gate.forhonor.R
import me.seroperson.gate.forhonor.login.LoginViewModel
import me.seroperson.gate.forhonor.model.Model
import me.seroperson.gate.forhonor.service.GlobalStatisticsElement

class GlobalFragment : Fragment() {

    private val loginViewModel: LoginViewModel by activityViewModels()
    private val globalViewModel: GlobalViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        inflater.inflate(R.layout.fragment_global, container, false).apply {
            ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.global_bar)) { v, insets ->
                v.setPaddingRelative(0, 0, 0, insets.systemWindowInsetBottom)
                insets.consumeSystemWindowInsets()
            }
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val navigationController = findNavController()

        globalViewModel.state.observe(this, Observer { state ->
            when(state) {
                is GlobalViewModel.GlobalState.HasData -> {
                    global_heroes.adapter = HeroAdapter(state.reply.statisticsList)
                    if(global_heroes.itemDecorationCount == 0) {
                        val divider = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
                        global_heroes.addItemDecoration(divider)
                    }
                }
                is GlobalViewModel.GlobalState.NoData -> {
                    val loginState = loginViewModel.state.value
                    if(loginState is LoginViewModel.LoginState.Authenticated) {
                        val name = loginState.user.name
                        globalViewModel.loadGlobal(name)
                    }
                }
            }
        })
        loginViewModel.state.observe(this, Observer { state ->
            when(state) {
                is LoginViewModel.LoginState.Unauthenticated -> {
                    navigationController.popBackStack(R.id.loginFragment, false)
                }
            }
        })
    }

    private inner class HeroAdapter(private val heroes: List<GlobalStatisticsElement>) :
        RecyclerView.Adapter<HeroAdapter.HeroViewHolder>() {

        inner class HeroViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val image: ImageView = view.findViewById(R.id.global_hero_image)
            val title: TextView = view.findViewById(R.id.global_hero_title)
            val level: TextView = view.findViewById(R.id.global_hero_level_value)
            val kills: TextView = view.findViewById(R.id.global_hero_kills_value)
            val deaths: TextView = view.findViewById(R.id.global_hero_deaths_value)
            val assists: TextView = view.findViewById(R.id.global_hero_assists_value)
            val wins: TextView = view.findViewById(R.id.global_hero_wins_value)
            val looses: TextView = view.findViewById(R.id.global_hero_looses_value)
            val online: TextView = view.findViewById(R.id.global_hero_online_value)
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            HeroViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.fragment_global_hero, parent, false)
            )
        override fun onBindViewHolder(holder: HeroAdapter.HeroViewHolder, position: Int) {
            val hero = heroes[position].hero
            holder.image.setImageDrawable(resources.getDrawable(when(hero) {
                Model.Hero.Warden -> R.drawable.icon_hero_warden
                Model.Hero.PeaceKeeper -> R.drawable.icon_hero_peacekeeper
                Model.Hero.Centurion -> R.drawable.icon_hero_centurion
                Model.Hero.Gladiator -> R.drawable.icon_hero_gladiator
                Model.Hero.Lawbringer -> R.drawable.icon_hero_lawbringer
                Model.Hero.Conqueror -> R.drawable.icon_hero_conq
                Model.Hero.Orochi -> R.drawable.icon_hero_orochi
                Model.Hero.Kensei -> R.drawable.icon_hero_kensei
                Model.Hero.Nobushi -> R.drawable.icon_hero_nobushi
                Model.Hero.Shinobi -> R.drawable.icon_hero_shinobi
                Model.Hero.Aramusha -> R.drawable.icon_hero_aramusha
                Model.Hero.Shugoki -> R.drawable.icon_hero_pudge
                Model.Hero.Berserker -> R.drawable.icon_hero_zerk
                Model.Hero.Raider -> R.drawable.icon_hero_raider
                Model.Hero.Highlander -> R.drawable.icon_hero_highlander
                Model.Hero.Shaman -> R.drawable.icon_hero_shaman
                Model.Hero.Valkyrie -> R.drawable.icon_hero_valk
                Model.Hero.Warlord -> R.drawable.icon_hero_warlord
                Model.Hero.Nuxia -> R.drawable.icon_hero_nuxia
                Model.Hero.Tiandi -> R.drawable.icon_hero_tiandi
                Model.Hero.JJ -> R.drawable.icon_hero_jj
                Model.Hero.Shaolin -> R.drawable.icon_hero_shaolin
                Model.Hero.BlackPrior -> R.drawable.icon_hero_peacekeeper // todo
                else -> TODO()
            }, null))
            holder.title.text = hero.name
            holder.level.text = heroes[position].level.toString()
            holder.kills.text = heroes[position].kills.toString()
            holder.deaths.text = heroes[position].deaths.toString()
            holder.assists.text = heroes[position].assists.toString()
            holder.wins.text = heroes[position].wins.toString()
            holder.looses.text = heroes[position].losses.toString()
            holder.online.text = heroes[position].online.toString()
        }
        override fun getItemCount() = heroes.size
    }
}
