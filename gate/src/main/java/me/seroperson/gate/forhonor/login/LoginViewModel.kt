package me.seroperson.gate.forhonor.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.grpc.ManagedChannelBuilder
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import me.seroperson.gate.forhonor.BuildConfig
import me.seroperson.gate.forhonor.RealError
import me.seroperson.gate.forhonor.service.MainServiceGrpc
import me.seroperson.gate.forhonor.service.TrackRequest
import java.util.concurrent.TimeUnit

class LoginViewModel : ViewModel() {

    val state = MutableLiveData<LoginState>()

    init {
        state.value = LoginState.Unauthenticated
    }

    fun loadUser(username: String) {
        Flowable.fromCallable {
            val channel = ManagedChannelBuilder
                .forAddress(BuildConfig.BACKEND_HOST, BuildConfig.BACKEND_PORT)
                .usePlaintext()
                .build()
            try {
                val stub = MainServiceGrpc.newBlockingStub(channel)
                val request = TrackRequest.newBuilder()
                    .setUsername(username)
                    .build()
                stub.trackUser(request)
            } catch(ex: Exception) {
                throw ex
            } finally {
                channel
                    .shutdown()
                    .awaitTermination(1, TimeUnit.SECONDS)
            }
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { User(it.username) }
            .subscribe({
                state.value = LoginState.Authenticated(it)
            }, {
                state.value = LoginState.Error(RealError(1))
                it.printStackTrace()
            })
    }

    sealed class LoginState {
        data class Authenticated(val user: User) : LoginState()
        data class Error(val error: RealError) : LoginState()
        object Unauthenticated : LoginState()
    }

}