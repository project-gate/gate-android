package me.seroperson.gate.forhonor.login

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo.IME_ACTION_DONE
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_login.*
import me.seroperson.gate.forhonor.R

class LoginFragment : Fragment() {

    private val model: LoginViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        inflater.inflate(R.layout.fragment_login, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val navigationController = findNavController()

        model.state.observe(this, Observer { state ->
            when(state) {
                is LoginViewModel.LoginState.Authenticated -> {
                    findLoadingBar()?.visibility = View.GONE
                    navigationController.navigate(R.id.action_login_to_profile)
                }
                is LoginViewModel.LoginState.Error -> {
                    findLoadingBar()?.visibility = View.GONE
                    login_input_layout.error = getString(R.string.login_input_layout_error_network)
                }
            }
        })

        login_input_field.setOnEditorActionListener { v, actionId, event: KeyEvent? ->
            if(actionId == IME_ACTION_DONE && (event == null || event.action == KeyEvent.ACTION_UP)) {
                val imm = getSystemService(v.context, InputMethodManager::class.java)!!
                imm.hideSoftInputFromWindow(v.windowToken, 0)

                val username = v.findViewById<EditText>(R.id.login_input_field).text?.toString()
                require(username != null)

                val validationResult = doValidation(username)
                if(validationResult is ValidationResult.Valid) {
                    login_input_layout.error = null
                    doLogin(username)
                } else {
                    when(validationResult) {
                        is ValidationResult.Empty -> login_input_layout.error = getString(R.string.login_input_layout_error_empty)
                    }
                }
                true
            } else {
                false
            }
        }
        login_action.setOnClickListener {
            val username = login_input_field.text?.toString()
            require(username != null)

            val validationResult = doValidation(username)
            if(validationResult is ValidationResult.Valid) {
                login_input_layout.error = null
                findLoadingBar()?.visibility = View.VISIBLE
                doLogin(username)
            } else {
                when(validationResult) {
                    is ValidationResult.Empty -> login_input_layout.error = getString(R.string.login_input_layout_error_empty)
                }
            }
        }
    }

    private fun findLoadingBar(): View? = requireActivity().findViewById(R.id.loading_bar)

    sealed class ValidationResult {
        object Empty : ValidationResult()
        object Valid : ValidationResult()
    }

    private fun doValidation(username: String): ValidationResult {
        return when {
            username.isBlank() -> ValidationResult.Empty
            else -> ValidationResult.Valid
        }
    }

    private fun doLogin(username: String) {
        model.loadUser(username)
    }

}