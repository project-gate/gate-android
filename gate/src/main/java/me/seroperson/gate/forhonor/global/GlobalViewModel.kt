package me.seroperson.gate.forhonor.global

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.grpc.ManagedChannelBuilder
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import me.seroperson.gate.forhonor.BuildConfig
import me.seroperson.gate.forhonor.RealError
import me.seroperson.gate.forhonor.service.GlobalStatisticsReply
import me.seroperson.gate.forhonor.service.GlobalStatisticsRequest
import me.seroperson.gate.forhonor.service.MainServiceGrpc
import java.util.concurrent.TimeUnit

class GlobalViewModel : ViewModel() {

    val state = MutableLiveData<GlobalState>()

    init {
        state.value = GlobalState.NoData
    }

    fun loadGlobal(username: String) {
        Flowable.fromCallable {
            val channel = ManagedChannelBuilder
                .forAddress(BuildConfig.BACKEND_HOST, BuildConfig.BACKEND_PORT)
                .usePlaintext()
                .build()
            try {
                val stub = MainServiceGrpc.newBlockingStub(channel)
                val request = GlobalStatisticsRequest.newBuilder()
                    .setName(username)
                    .build()
                stub.getGlobalStatistics(request)
            } catch(ex: Exception) {
                throw ex
            } finally {
                channel
                    .shutdown()
                    .awaitTermination(1, TimeUnit.SECONDS)
            }
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                state.value = GlobalState.HasData(it)
            }, {
                state.value = GlobalState.Error(RealError(1))
                it.printStackTrace()
            })
    }

    sealed class GlobalState {
        data class HasData(val reply: GlobalStatisticsReply) : GlobalState()
        data class Error(val error: RealError) : GlobalState()
        object NoData : GlobalState()
    }

}